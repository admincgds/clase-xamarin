﻿using System;
using System.Collections.Generic;
using CursoXamarin.ViewModels;
using Xamarin.Forms;

namespace CursoXamarin.Views
{
    public partial class RegistroView : ContentPage
    {
        public RegistroView()
        {
            InitializeComponent();
            BindingContext = new LoginViewModel();
        }
    }
}
