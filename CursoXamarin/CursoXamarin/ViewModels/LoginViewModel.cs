﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using CursoXamarin.Models;
using CursoXamarin.Views;
using Xamarin.Forms;

namespace CursoXamarin.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {

        #region Propiedades

        private UserModel _User = new UserModel();

        public UserModel User
        {
            get
            {
                return _User;
            }

            set
            {
                _User = value;
                OnPropertyChanged("User");
            }
        }


        #endregion

        #region Comandos

        public ICommand LoginCommand { get; set; }
        public ICommand IrRegistrarCommand { get; set; }
        public ICommand RegresarLoginCommand { get; set; }


        #endregion

        public LoginViewModel()
        {

            LoginCommand = new Command(Login);
            IrRegistrarCommand = new Command(IrRegistrar);
            RegresarLoginCommand = new Command(Regresar);
        }

        public async void Login()
        {
            try
            {
                if(User.Email.ToLower() == "test@test.com" && User.Password == "1234")
                {
                    //Application.Current.MainPage = new HomeView();
                    NavigationPage navigation = new NavigationPage(new HomeView());

                    //Application.Current.MainPage = new MasterDetailPage { Master = new MenuView() , Detail = new HomeView() };
                    Application.Current.MainPage = new MasterDetailPage { Master = new MenuView() , Detail = navigation };

                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Error", "Credenciales incorrectas", "OK");
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Ha ocurrido un error: " + ex.Message, "OK");
            }

        }

        public async void IrRegistrar()
        {
            Application.Current.MainPage = new RegistroView();
        }

        public async void Regresar()
        {
            Application.Current.MainPage = new LoginView();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
