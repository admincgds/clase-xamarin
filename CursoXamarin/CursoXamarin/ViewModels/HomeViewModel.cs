﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using CursoXamarin.Models;
using CursoXamarin.Views;
using Xamarin.Forms;

namespace CursoXamarin.ViewModels
{
    public class HomeViewModel : INotifyPropertyChanged
    {

        //Propiedades
        private ObservableCollection<DoctorModel> _lstDoctors = new ObservableCollection<DoctorModel>();

        public ObservableCollection<DoctorModel> lstDoctors
        {
            get
            {
                return _lstDoctors;
            }
            set
            {
                _lstDoctors = value;
                OnPropertyChanged("lstDoctors");
            }
        }

        private DoctorModel _CurrentDoctor = new DoctorModel();

        public DoctorModel CurrentDoctor
        {
            get
            {
                return _CurrentDoctor;
            }

            set
            {
                _CurrentDoctor = value;
                OnPropertyChanged("CurrentDoctor");
            }
        }

        #region Singleton

        private static HomeViewModel instance = null;

        private HomeViewModel()
        {
            InitClass();
            InitCommand();

        }

        public static HomeViewModel GetInstance()
        {
            if(instance == null)
            {
                instance = new HomeViewModel();
            } 

            return instance;
        }

        public static void DeleteInstance()
        {
            if(instance != null)
            {
                instance = null;
            }
        }

        #endregion

        #region Comandos

        public ICommand EnterDoctorDetailCommand { get; set; }

        public ICommand EnterDoctorPostCommand { get; set; }


        #endregion


      

        public async void InitClass()
        {
            lstDoctors = await DoctorModel.GetAllDoctos();
        }

        public async void GetDetail(string id)
        {
            CurrentDoctor = await DoctorModel.GetDetailDoctor(id);
        }

        public void InitCommand()
        {
            EnterDoctorDetailCommand = new Command<DoctorModel>(EnterDoctorDetail);
            EnterDoctorPostCommand = new Command<string>(EnterDoctorPost);
        }

        public void EnterDoctorDetail(DoctorModel doctor)
        {
            //_CurrentDoctor = doctor;

            GetDetail(doctor.Id);


            ((MasterDetailPage)App.Current.MainPage).Detail.Navigation.PushAsync(new DetailDoctorView());
            
        }

        public void EnterDoctorPost(string id)
        {
            ((MasterDetailPage)App.Current.MainPage).Detail.Navigation.PushAsync(new PostView(id));
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if(propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


    }
}
