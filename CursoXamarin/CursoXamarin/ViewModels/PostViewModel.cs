﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using CursoXamarin.Models;
using CursoXamarin.Views;
using Xamarin.Forms;
namespace CursoXamarin.ViewModels
{
    public class PostViewModel : INotifyPropertyChanged
    {

        //Propiedades
        private ObservableCollection<PostModel> _lstPost = new ObservableCollection<PostModel>();

        public ObservableCollection<PostModel> lstPost
        {
            get
            {
                return _lstPost;
            }
            set
            {
                _lstPost = value;
                OnPropertyChanged("lstPost");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public PostViewModel(string id)
        {
            InitClass(id);
        }


        public async void InitClass(string id)
        {
            lstPost = await PostModel.GetAllDoctorPost(id);
        }



    }
}
