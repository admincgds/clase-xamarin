﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using CursoXamarin.Models;
using CursoXamarin.Views;
using Xamarin.Forms;

namespace CursoXamarin.ViewModels
{
    public class MenuViewModel : INotifyPropertyChanged
    {

        //Propiedades
        private ObservableCollection<MenuModel> _lstMenu = new ObservableCollection<MenuModel>();

        public ObservableCollection<MenuModel> lstMenu
        {
            get
            {
                return _lstMenu;
            }
            set
            {
                _lstMenu = value;
                OnPropertyChanged("lstMenu");
            }
        }

        #region Comandos

        public ICommand CerrarCommand { get; set; }

        #endregion

        public MenuViewModel()
        {

            lstMenu.Add(new MenuModel { Id = 1, Name = "Opción 1",  Icon = "https://hospitalchiriqui.com/wp-content/uploads/2021/03/medico.png" });
            lstMenu.Add(new MenuModel { Id = 2, Name = "Opción 2",  Icon = "https://image.flaticon.com/icons/png/512/387/387628.png" });

            CerrarCommand = new Command(Cerrar);

        }

        public async void Cerrar()
        {

            var action = await Application.Current.MainPage.DisplayAlert("Cerrar Sesión", "¿Está seguro que desea cerrar la sesión?", "Si", "No");
            if (action)
            {
                Application.Current.MainPage = new LoginView();
            } 

        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
