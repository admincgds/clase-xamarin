﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CursoXamarin.Models
{
    public class DoctorModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
        public string FullName => $"{Title}. {FirstName} {LastName}";
        public Location Location { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Phone { get; set; }
        public string RegisterDate { get; set; }
        public string UpdatedAt { get; set; }



        public DoctorModel()
        {
        }

        public async static Task<ObservableCollection<DoctorModel>> GetAllDoctos()
        {
            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri("https://dummyapi.io/data/api/user");

                client.DefaultRequestHeaders.Add("app-id", "610a939630b8d938becfd282");

                HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);

                string answer = await response.Content.ReadAsStringAsync();

                ResponseDoctorModel responseObject = JsonConvert.DeserializeObject<ResponseDoctorModel>(answer);

                return responseObject.data;

            }
        }


        public async static Task<DoctorModel> GetDetailDoctor(String idDoctor)
        {
            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri("https://dummyapi.io/data/api/user/"+idDoctor);

                client.DefaultRequestHeaders.Add("app-id", "610a939630b8d938becfd282");

                HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);

                string answer = await response.Content.ReadAsStringAsync();

                DoctorModel responseObject = JsonConvert.DeserializeObject<DoctorModel>(answer);

                return responseObject;

            }
        }






    }
}
