﻿using System;
namespace CursoXamarin.Models
{
    public class Owner
    {

        public String id { get; set; }
        public String title { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String email { get; set; }
        public String picture { get; set; }

        public Owner()
        {
        }
    }
}
