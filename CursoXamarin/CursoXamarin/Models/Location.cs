﻿using System;
namespace CursoXamarin.Models
{
    public class Location
    {

        public String Street { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Country { get; set; }
        public String Timezone { get; set; }

        public Location()
        {
        }
    }
}
