﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CursoXamarin.Models
{
    public class PostModel
    {

        public Owner owner { get; set; }
        public string id { get; set; }
        public string image { get; set; }
        public string publishDate { get; set; }
        public string text { get; set; }
        public string link { get; set; }
        public int likes { get; set; }


        public PostModel()
        {
        }


        public async static Task<ObservableCollection<PostModel>> GetAllDoctorPost(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri("https://dummyapi.io/data/api/user/"+id+"/post");

                client.DefaultRequestHeaders.Add("app-id", "610a939630b8d938becfd282");

                HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);

                string answer = await response.Content.ReadAsStringAsync();

                ResponsePostModel responseObject = JsonConvert.DeserializeObject<ResponsePostModel>(answer);

                return responseObject.data;

            }
        }


    }
}
